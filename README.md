The PeeNaPle "Xtd" for extended, is an almost all in one version of the [PeeNaPle V1.4b](https://gitlab.com/CrazzyFrenchDude/peenaple_v1.1b)!
It comes at a very competitive low price tag and is also a kit! With a common board 
shown above and a "Xtd" side board for either 3D Printing or for Open PnP!
The sharp eyes will notice no power input or management on the main board, this is because
it's on the "Xtd" side board!

*3D Preview rendering is for design positioning only and is subject for change!

